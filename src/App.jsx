import { Routes, Route } from "@solidjs/router";
import "flowbite";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Footer from "./components/Footer";

export default function App() {
  return (
    <div class="font-body">
      <Navbar />
      <Routes>
        <Route path="/" component={Home} />
      </Routes>
      <Footer />
    </div>
  );
}
