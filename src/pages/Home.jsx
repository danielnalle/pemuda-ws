import Carousel from "../components/Carousel";
import Greeting from "../components/Greeting";
import Heading from "../components/Heading";
import Card from "../components/Card";
import Dewi_Sundari from "../assets/profile/Dewi_Sundari.jpg";
import Putu_Daniel from "../assets/profile/Putu_Daniel.jpg";
import Edgina_Devina from "../assets/profile/Edgina_Devina.jpg";
import WSportCompetition from "../assets/program-agenda/WsportCompetition.jpg";
import IbadahPadang from "../assets/program-agenda/IbadahPadang.jpg";
import IbadahRutin from "../assets/program-agenda/IbadahRutin.jpg";
import PelatihanMinatBakat from "../assets/program-agenda/PelatihanMinatBakat.jpg";
import RapatPengurus from "../assets/program-agenda/RapatPengurus.jpg";
import Footer from "../components/Footer";

export default function Home() {
  return (
    <>
      <Carousel />
      <Heading title="Tentang Kami" anchor="tentang-kami">
        <p class="mb-6 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 xl:px-48 dark:text-gray-400">
          Pemuda GKPB Jemaat Widhi Satya lahir dari kebutuhan untuk menampung
          aspirasi, kreasi dan inovasi pemuda/i yang berjemaat di GKPB Widhi
          Satya.
        </p>
      </Heading>
      <Greeting img_name={Dewi_Sundari} name="Pdt. Dewi Sundari, S.Si Teol">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Error quos eius
        ratione dolorem ea voluptas unde eligendi, accusantium illum cum quae
        necessitatibus numquam beatae autem earum nemo. Architecto sequi
        repellat maiores illum suscipit labore quibusdam mollitia provident!
        Cumque deserunt, libero ad aut at odit voluptatem pariatur sunt facere
        voluptate earum.
      </Greeting>
      <Greeting
        img_name={Putu_Daniel}
        name="Pnt. I Putu Daniel Widhyadi Septiana, S.Pd."
      >
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur earum
        adipisci voluptate excepturi perspiciatis nobis corrupti ea veniam
        officiis asperiores sunt consequuntur omnis natus quia, consectetur
        obcaecati eum beatae quod debitis fuga maiores qui quis. Earum veniam
        aliquam iusto, tempora non omnis, hic officiis dolore architecto nihil
        quas optio aliquid in amet sint officia numquam doloremque quae maiores.
        Recusandae, voluptatem.
      </Greeting>
      <Greeting img_name={Edgina_Devina} name="Edgina Devina">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo,
        blanditiis perferendis. Aspernatur cupiditate incidunt vitae voluptate?
        Rem, optio adipisci neque, minus consequatur accusantium, rerum ad
        accusamus praesentium dolorum quia at veniam tenetur officiis.
        Consectetur voluptas quisquam cum consequatur debitis ratione atque
        velit iure qui sint fuga quo ad at, itaque laudantium, sit voluptates
        deserunt? Esse aliquam nisi dolore quis, nulla repellendus est facere
        alias, odio voluptatibus ipsam suscipit qui nostrum.
      </Greeting>
      <Heading title="Visi & Misi" anchor="visi-misi">
        <div class="mb-10">
          <h2 class="sub-heading">Visi</h2>
          <p class="capitalize mb-6 text-lg font-normal text-slate-500 lg:text-xl sm:px-16 xl:px-48 dark:text-slate-400">
            Bumi bersukacita dalam damai sejahtera
          </p>
        </div>
        <div>
          <h2 class="sub-heading">Misi</h2>
          <p class="capitalize mb-6 text-lg font-normal text-slate-500 lg:text-xl sm:px-16 xl:px-48 dark:text-slate-400">
            Membangun peradaban yg dijiwai oleh kasih terhadap Tuhan, sesama dan
            lingkungan dalam rangka menjadi berkat dan terang bangsa - bangsa.
          </p>
        </div>
      </Heading>
      <Heading title="Program & Agenda" anchor="program-agenda">
        <div class="pb-16">
          <h2 class="sub-heading">Program</h2>
          <div class="lg:px-20 xl:px-48 flex flex-wrap xl:flex-nowrap justify-center lg:justify-between">
            <Card img={WSportCompetition} title="WSport Competition">
              Kami selalu berusaha untuk menyelenggarakan kompetisi dalam bidang
              olahraga setiap tahunnya, nantikan kejutan kami!
            </Card>
            <Card img={IbadahPadang} title="Ibadah Padang">
              Kami mengingatrayakan momen-momen tertentu dengan berkumpul dan
              memuliakan nama Tuhan!
            </Card>
          </div>
        </div>
        <div>
          <h2 class="sub-heading">Agenda</h2>
          <div class="lg:px-14 xl:px-0 flex flex-wrap justify-evenly">
            <Card img={PelatihanMinatBakat} title="Pelatihan Minat Bakat">
              Kami menyediakan media bagi pemuda yang ingin melatih talentnya
              untuk memuliakan nama Tuhan, mari bergabung!
            </Card>
            <Card img={IbadahRutin} title="Ibadah Rutin">
              Ibadah kami dilaksanakan setiap hari Sabtu pukul 19.30 WITA, ayo
              bergabung bersama kami!
            </Card>
            <Card img={RapatPengurus} title="Rapat Pengurus">
              Pengurus ingin maksimal dalam melayani sehingga persiapan dan
              evaluasi tentu menjadi bagian untuk menuju yang terbaik!
            </Card>
          </div>
        </div>
      </Heading>
    </>
  );
}
