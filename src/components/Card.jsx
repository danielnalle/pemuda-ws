// import WSportCompetition from "../assets/program-agenda/WsportCompetition.jpg";

export default function Card(props) {
  const { img, title, children } = props;

  return (
    <div class="max-w-sm text-left mb-4 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <a href="#" class="block">
        <img class="w-96 h-56 object-cover rounded-t-lg" src={img} alt="" />
      </a>
      <div class="p-5">
        <a href="#">
          <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            {title}
          </h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
          {children}
        </p>
      </div>
    </div>
  );
}
