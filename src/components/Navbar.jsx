import { createSignal } from "solid-js";
import { A } from "@solidjs/router";
import pemudaWSLogo from "../assets/logo/PemudaWS.png";

export default function Navbar() {
  const [isHidden, setIsHidden] = createSignal(true);

  const toggleNavbar = () => {
    setIsHidden(!isHidden());
  };

  return (
    <nav class="border-slate-200 bg-slate-100 dark:bg-slate-800 dark:border-slate-700">
      <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto py-4 px-content">
        <a href="#" class="flex items-center">
          <img
            src={pemudaWSLogo}
            class="w-8 h-8 mr-3"
            alt="Pemuda GKPB Widhi Satya Logo"
          />
          <span class="self-center text-2xl font-bold uppercase whitespace-nowrap dark:text-white">
            Pemuda WS
          </span>
        </a>
        <button
          onClick={toggleNavbar}
          data-collapse-toggle="navbar-solid-bg"
          type="button"
          class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
          aria-controls="navbar-solid-bg"
          aria-expanded="false"
        >
          <span class="sr-only">Open main menu</span>
          <svg
            class="w-5 h-5"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 17 14"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M1 1h15M1 7h15M1 13h15"
            />
          </svg>
        </button>
        <div
          class="hidden w-full lg:block lg:w-auto"
          classList={{ hidden: isHidden() }}
          id="navbar-solid-bg"
        >
          <ul class="flex flex-col font-medium mt-4 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-transparent dark:bg-gray-800 md:dark:bg-transparent dark:border-gray-700">
            <li>
              <A
                href="#tentang-kami"
                class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500 dark:bg-blue-600 md:dark:bg-transparent"
                aria-current="page"
              >
                Tentang Kami
              </A>
            </li>
            <li>
              <a
                href="#"
                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
              >
                Program & Agenda
              </a>
            </li>
            <li>
              <a
                href="#"
                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
              >
                Visi & Misi
              </a>
            </li>
            <li>
              <A
                href="/daftar"
                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
              >
                Daftar 3X3
              </A>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
