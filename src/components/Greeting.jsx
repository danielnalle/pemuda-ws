export default function Greeting(props) {
  const { name, img_name, children } = props;

  return (
    <div class="px-content py-12 odd:bg-slate-100 flex flex-col lg:flex-row lg:odd:flex-row-reverse justify-between items-center">
      <div class="flex justify-center mb-8 lg:mb-0">
        <img
          class="h-auto w-80 md:w-96 lg:max-w-md rounded-lg"
          src={img_name}
          alt={name}
        />
      </div>
      <div class="text-center lg:text-justify lg:px-16">
        <h3 class="text-xl md:text-2xl lg:text-3xl font-bold dark:text-white pb-6 lg:pb-10">
          {name}
        </h3>
        <p class="mb-3 text-md lg:text-xl text-gray-500 dark:text-gray-400">
          {children}
        </p>
      </div>
    </div>
  );
}
