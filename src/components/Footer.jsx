import PemudaWS from "../assets/logo/PemudaWS.png";
import GKPB from "../assets/logo/GKPB.png";
import WilkotPusat from "../assets/logo/Pusat.png";
import Wilkot from "../assets/logo/Wilkot.png";
import { A } from "@solidjs/router";

export default function Footer() {
  return (
    <footer class="bg-slate-100 dark:bg-gray-900" id="kontak-kami">
      <div class="px-content py-6 lg:py-8">
        <div class="md:flex md:justify-between">
          <div class="flex items-start mb-6 md:mb-0">
            <A href="https://pemudaws.com/" class="flex items-center">
              <img src={GKPB} class="h-24 mr-3" alt="GKPB Logo" />
            </A>
            <A href="https://pemudaws.com/" class="flex items-center">
              <img src={PemudaWS} class="h-24 mr-3" alt="GKPB Pemuda WS Logo" />
            </A>
            <A href="https://pemudaws.com/" class="flex items-center">
              <img src={WilkotPusat} class="h-24" alt="Wilkot Pusat Logo" />
            </A>
            <A href="https://pemudaws.com/" class="flex items-center">
              <img src={Wilkot} class="h-24" alt="Wilkot Logo" />
            </A>
          </div>
          <div class="grid grid-cols-1 md:grid-cols-2 gap-8 sm:gap-6">
            <div>
              <h2 class="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Kontak
              </h2>
              <ul class="text-gray-500 dark:text-gray-400 font-medium">
                <li class="mb-4 flex items-center">
                  <span class="pr-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      class="w-4 h-w-4"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M1.5 4.5a3 3 0 013-3h1.372c.86 0 1.61.586 1.819 1.42l1.105 4.423a1.875 1.875 0 01-.694 1.955l-1.293.97c-.135.101-.164.249-.126.352a11.285 11.285 0 006.697 6.697c.103.038.25.009.352-.126l.97-1.293a1.875 1.875 0 011.955-.694l4.423 1.105c.834.209 1.42.959 1.42 1.82V19.5a3 3 0 01-3 3h-2.25C8.552 22.5 1.5 15.448 1.5 6.75V4.5z"
                        clip-rule="evenodd"
                      />
                    </svg>
                  </span>
                  {"(Egik)"} 0878-6002-3532
                </li>
                <li class="mb-4 flex items-center">
                  <span class="pr-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 24 24"
                      class="w-4 h-4"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M 8 3 C 5.239 3 3 5.239 3 8 L 3 16 C 3 18.761 5.239 21 8 21 L 16 21 C 18.761 21 21 18.761 21 16 L 21 8 C 21 5.239 18.761 3 16 3 L 8 3 z M 18 5 C 18.552 5 19 5.448 19 6 C 19 6.552 18.552 7 18 7 C 17.448 7 17 6.552 17 6 C 17 5.448 17.448 5 18 5 z M 12 7 C 14.761 7 17 9.239 17 12 C 17 14.761 14.761 17 12 17 C 9.239 17 7 14.761 7 12 C 7 9.239 9.239 7 12 7 z M 12 9 A 3 3 0 0 0 9 12 A 3 3 0 0 0 12 15 A 3 3 0 0 0 15 12 A 3 3 0 0 0 12 9 z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                  </span>
                  @pemuda.ws
                </li>
                <li class="flex items-center">
                  <span class="pr-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="50"
                      height="50"
                      viewBox="0 0 50 50"
                      fill="currentColor"
                      class="w-4 h-4"
                    >
                      <path d="M 5.5 7 C 3.019531 7 1 9.019531 1 11.5 L 1 11.925781 L 25 29 L 49 11.925781 L 49 11.5 C 49 9.019531 46.980469 7 44.5 7 Z M 6.351563 9 L 43.644531 9 L 25 22 Z M 1 14.027344 L 1 38.5 C 1 40.980469 3.019531 43 5.5 43 L 44.5 43 C 46.980469 43 49 40.980469 49 38.5 L 49 14.027344 L 43 18.296875 L 43 41 L 7 41 L 7 18.296875 Z"></path>
                    </svg>
                  </span>
                  pemuda.widhisatya@gmail.com
                </li>
              </ul>
            </div>
            <div>
              <h2 class="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Menu
              </h2>
              <ul class="text-gray-500 dark:text-gray-400 font-medium">
                <li class="mb-4">
                  <A href="#tentang-kami" class="hover:underline">
                    Tentang Kami
                  </A>
                </li>
                <li class="mb-4">
                  <A href="#visi-misi" class="hover:underline">
                    Visi & Misi
                  </A>
                </li>
                <li class="mb-4">
                  <A href="#program-agenda" class="hover:underline">
                    Program & Agenda
                  </A>
                </li>
                <li class="mb-4">
                  <A href="#daftar" class="hover:underline">
                    Daftar 3x3
                  </A>
                </li>
                <li>
                  <A href="#kontak-kami" class="hover:underline">
                    Kontak Kami
                  </A>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <hr class="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
        <div class="text-center">
          <span class="text-sm text-gray-500 sm:text-center dark:text-gray-400">
            © 2023{" "}
            <a href="https://flowbite.com/" class="hover:underline">
              PP Kristiyasa GKPB Jemaat Widhi Satya
            </a>
          </span>
        </div>
      </div>
    </footer>
  );
}
