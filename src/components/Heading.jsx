export default function Heading(props) {
  const { title, anchor, children } = props;
  return (
    <div class="text-center py-32 px-content" id={anchor}>
      <h1 class="uppercase pb-20 text-3xl font-extrabold leading-none tracking-tight text-gray-900 md:text-4xl lg:text-5xl dark:text-white">
        {title}
      </h1>
      {children}
    </div>
  );
}
